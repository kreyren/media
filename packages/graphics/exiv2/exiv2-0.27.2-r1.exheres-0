# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Exiv2 tag=v${PV} ] cmake

SUMMARY="Exiv2 is a C++ library and a command line utility to manage image metadata"
HOMEPAGE+=" https://www.${PN}.org"

UPSTREAM_CHANGELOG="https://www.${PN}.org/changelog.html"
UPSTREAM_RELEASE_NOTES="https://www.${PN}.org/whatsnew.html"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    curl [[ description = [ Support accessing remote media via HTTP/HTTPS ] ]]
    sftp [[ description = [ Support accessing remote media via SSH/SFTP ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/expat
        curl? ( net-misc/curl )
        sftp? ( net-libs/libssh )
    test:
        dev-cpp/gtest
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DEXIV2_BUILD_DOC:BOOL=FALSE
    -DEXIV2_BUILD_EXIV2_COMMAND:BOOL=TRUE
    -DEXIV2_BUILD_SAMPLES:BOOL=FALSE
    -DEXIV2_ENABLE_LENSDATA:BOOL=TRUE
    -DEXIV2_ENABLE_NLS:BOOL=TRUE
    -DEXIV2_ENABLE_PNG:BOOL=TRUE
    -DEXIV2_ENABLE_PRINTUCS2:BOOL=TRUE
    -DEXIV2_ENABLE_VIDEO:BOOL=TRUE
    -DEXIV2_ENABLE_WEBREADY:BOOL=TRUE
    -DEXIV2_ENABLE_WIN_UNICODE:BOOL=FALSE
    -DEXIV2_ENABLE_XMP:BOOL=TRUE
    -DEXIV2_TEAM_PACKAGING:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'curl EXIV2_ENABLE_CURL'
    'sftp EXIV2_ENABLE_SSH'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DEXIV2_BUILD_UNIT_TESTS:BOOL=TRUE -DEXIV2_BUILD_UNIT_TESTS:BOOL=FALSE'
)

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/93a5f2a2bcc751ff52240935d2a27bfe64e27a10.patch
    "${FILES}"/683451567284005cd24e1ccb0a76ca401000968b.patch
    "${FILES}"/0890d66a6e31350c3e181816ee9c7bb561f0900d.patch
)

src_prepare() {
    cmake_src_prepare

    # TODO: fix upstream ...again
    edo sed \
        -e 's:EXV_LOCALEDIR="/../:EXV_LOCALEDIR=":g' \
        -i src/CMakeLists.txt
}

src_test() {
    edo ./bin/unit_tests
}

src_install() {
    cmake_src_install

    # TODO: fix upstream
    edo mv "${IMAGE}"/usr/{$(exhost --target)/,}share/locale
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
}

