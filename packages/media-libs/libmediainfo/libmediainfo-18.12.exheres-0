# Copyright 2013 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="MediaInfo libraries"
HOMEPAGE="https://mediaarea.net/mediainfo"
DOWNLOADS="https://mediaarea.net/download/source/${PN}/${PV}/${PN}_${PV}.tar.xz"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

# tests fetch a raw file from github via wget
# wget https://github.com/MediaArea/MediaAreaXml/raw/master/mediatrace.xsd
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/tinyxml2
        media-libs/libzen
"

WORK=${WORKBASE}/MediaInfoLib/Project/GNU/Library

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-shared
    --disable-static
    --disable-staticlibs
    --with-libtinyxml2
    --without-libcurl
    --without-libmms
)

src_prepare() {
    # TODO: report upstream
    edo sed \
        -e "s:pkg-config:$(exhost --tool-prefix)&:g" \
        -i configure.ac

    autotools_src_prepare
}

src_install() {
    default

    # Headers which mediainfo needs, but which aren't installed by automake, for some reason
    for x in ./ Archive Audio Duplicate Export Image Multiple Reader Tag Text Video; do
        insinto /usr/$(exhost --target)/include/MediaInfo/${x}
        doins "${WORKBASE}"/MediaInfoLib/Source/MediaInfo/${x}/*.h
    done
}

