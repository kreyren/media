# Copyright 2012-2015 Johannes Nixdorf <mixi@exherbo.org>
# Based in part upon 'mplayer2.exlib', which is
#   Copyright 2011 Elias Pipping <pipping@exherbo.org>
#   Copyright 2011 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# last checked revision: 517489814d6bc4837a1c24f7601a77136564fb7a

require freedesktop-desktop github [ user="mpv-player" tag="v${PV}" ] gtk-icon-cache waf \
        lua [ multibuild=false whitelist="5.1 5.2" with_opt=true ] bash-completion zsh-completion

export_exlib_phases src_prepare src_configure src_install pkg_postrm pkg_postinst

WAF_VER=2.0.9

SUMMARY="Video player based on MPlayer/mplayer2"
HOMEPAGE="https://mpv.io"
DOWNLOADS+=" https://waf.io/pub/release/waf-${WAF_VER}"

UPSTREAM_RELEASE_NOTES="https://github.com/mpv-player/mpv/releases"

LICENCES="GPL-2 GPL-3 LGPL-2.1"
SLOT="0"

# TODO: cuda-interop requires opengl OR vulkan
MYOPTIONS="
    alsa
    archive     [[ description = [ Adds support for reading media from zip files and more ] ]]
    bluray      [[ description = [ Adds support for video blurays ] ]]
    cd          [[ description = [ Adds support for audio CDs ] ]]
    cuda        [[ description = [ Adds support for NVIDIA hardware accelerated decoding using the CUDA API ] ]]
    drm         [[ description = [ Adds support for a video output directly to the framebuffer ] ]]
    dvd
    icc         [[ description = [ Adds support for using ICC profiles through lcms2 ] ]]
    jack        [[ description = [ Support for the Jack Audio Connection Kit ] ]]
    lua         [[ description = [ Adds lua scripting support and an onscreen controller ] ]]
    opengl
    pulseaudio
    rubberband  [[ description = [ Filter for time-stretching and pitch-shifting using librubberband ] ]]
    sndio       [[ description = [ Adds support for sound output through sndio (OpenBSD sound API, also ported to Linux) ] ]]
    va          [[ description = [ Adds support for decoding and presenting video using the Video Acceleration API ]
                   requires = [ providers: ffmpeg ] ]]
    vapoursynth [[ description = [ Adds support for vapoursynth filters ] ]]
    vdpau       [[ description = [ Adds support for presenting and decoding video using the VDPAU API (-vo=vdpau and -hwdec=vdpau) ]
                   requires = [ providers: ffmpeg ] ]]
    vulkan      [[ description = [ Support for the Vulkan API ] ]]
    wayland
    X
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    wayland? (
        va [[ requires = [ opengl ] ]]
    )
"

# disabled because i can't figure out waf
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*
        dev-python/docutils
        virtual/pkg-config
        X? ( x11-proto/xorgproto )
        cuda? ( media-libs/nv-codec-headers[>=8.2.15.7] )
        vulkan? ( sys-libs/vulkan-headers )
    build+run:
        media-libs/libass[fontconfig][>=0.12.1]
        sys-libs/ncurses
        sys-libs/zlib
        alsa? ( sys-sound/alsa-lib[>=1.0.18] )
        archive? ( app-arch/libarchive[>=3.4.0] )
        bluray? ( media-libs/libbluray[>=0.3.0] )
        cd? (
            dev-libs/libcdio
            dev-libs/libcdio-paranoia
        )
        drm? ( x11-dri/libdrm[>=2.4.74] )
        dvd? (
            media-libs/libdvdnav[>=4.2.0]
            media-libs/libdvdread[>=4.1.0]
        )
        jack? ( media-sound/jack-audio-connection-kit[>=0.120.1] )
        icc? ( media-libs/lcms2[>=2.6] )
        opengl? ( x11-dri/mesa[>=9.0.0][X?][wayland?] )
        providers:ffmpeg? ( media/ffmpeg[>=4.0][va?][vdpau?] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libav? ( media/libav[>=12] )
        pulseaudio? ( media-sound/pulseaudio[>=1.0] )
        rubberband? ( media-libs/rubberband[>=1.8.0] )
        sndio? ( sys-sound/sndio )
        vapoursynth? ( media-libs/vapoursynth[>=23][python] )
        va? ( x11-libs/libva[>=2.1.0][X?][wayland?] )
        vdpau? ( x11-libs/libvdpau[>=0.2] )
        vulkan? (
            media-libs/libplacebo[>=1.18.0]
            sys-libs/shaderc
            sys-libs/vulkan-loader
        )
        wayland? (
            sys-libs/wayland[>=1.15.0]
            sys-libs/wayland-protocols
            x11-libs/libxkbcommon[>=0.3.0]
        )
        X? (
            x11-libs/libX11[>=1.0.0]
            x11-libs/libXScrnSaver[>=1.0.0]
            x11-libs/libXext[>=1.0.0]
            x11-libs/libXinerama[>=1.0.0]
            x11-libs/libXrandr[>=1.2.0]
            x11-libs/libXv
        )
    run:
        cuda? ( providers:ffmpeg? ( media/ffmpeg[nvenc] ) )
    suggestion:
        lua? ( net-misc/youtube-dl[>=2015.02.23.1] [[ description = [ Support to play videos from YouTube and other video sites ] ]] )
"

mpv_src_prepare() {
    edo cp "${FETCHEDDIR}"/waf-${WAF_VER} waf
    chmod +x waf

    default
}

mpv_src_configure() {
    local args=(
        ## paths
        --prefix=/usr/$(exhost --target)
        --confdir=/etc/${PN}
        --libdir=/usr/$(exhost --target)/lib
        --datadir=/usr/share
        --docdir=/usr/share/doc/${PNVR}
        --htmldir=/usr/share/doc/${PNVR}/html
        --mandir=/usr/share/man

        ## doc
        --enable-manpage-build
        --disable-html-build
        --disable-pdf-build

        ## misc
        --enable-iconv
        --enable-jpeg
        --enable-libass
        --enable-libass-osd
        --enable-libmpv-shared
        --disable-ffmpeg-strict-abi
        --disable-javascript
        --disable-sdl2
        --disable-sdl2-gamepad
        --disable-ta-leak-report
        --disable-tests
        --disable-uchardet
        $(option lua --lua=${LUA_ABIS/./}fbsd --disable-lua)
        $(option_enable vapoursynth)

        ## sources
        --enable-dvbin
        --disable-libsmbclient
        $(option_enable archive libarchive)
        $(option_enable bluray libbluray)
        $(option_enable cd cdda)
        $(option_enable dvd dvdnav)
        $(option_enable jack)
        $(option_enable providers:ffmpeg libavdevice)

        ## audio
        $(option_enable rubberband)

        ## audio out
        --enable-oss-audio
        --disable-audiounit
        --disable-coreaudio
        --disable-openal
        --disable-opensles
        --disable-rsound
        --disable-sdl2-audio
        --disable-sndio
        --disable-wasapi
        --disable-zimg
        $(option_enable alsa)
        $(option_enable pulseaudio pulse)
        $(option_enable sndio)

        ## video
        --disable-caca
        $(option_enable icc lcms2)

        ## video out
        --disable-sdl2-video
        --disable-spirv-cross

        $(option_enable drm)
        $(option_enable drm drmprime)
        $(if option drm; then
            option_enable opengl egl-drm
            option_enable va vaapi-drm
        fi)

        $(option_enable opengl egl)
        $(option_enable opengl gl)
        $(option_enable opengl plain-gl)

        $(option_enable vulkan shaderc)
        $(option_enable vulkan)

        $(option_enable wayland)
        $(option_enable wayland wayland-protocols)
        $(option_enable wayland wayland-scanner)
        $(if option wayland; then
            option_enable opengl gl-wayland
            option_enable va vaapi-wayland
        fi)

        $(option_enable X x11)
        $(option_enable X xv)
        $(if option X; then
            option_enable opengl egl-x11
            option_enable opengl gl-x11
            option_enable va vaapi-x11
            if option opengl; then
                option_enable va vaapi-x-egl
                option_enable vdpau vdpau-gl-x11
            fi
        fi)

        ## platform
        --disable-android
        --disable-cocoa
        --disable-d3d11
        --disable-direct3d
        --disable-egl-angle
        --disable-gl-cocoa
        --disable-gl-win32
        --disable-ios-gl
        --disable-macos-10-11-features
        --disable-macos-10-12-2-features
        --disable-macos-10-14-features
        --disable-macos-cocoa-cb
        --disable-macos-media-player
        --disable-macos-touchbar
        --disable-rpi
        --disable-rpi-mmal
        --disable-swift
        --disable-tvos
        --disable-uwp
        --disable-videotoolbox-gl

        ## hwaccel
        --disable-d3d-hwaccel
        --disable-d3d9-hwaccel
        --disable-gl-dxinterop-d3d9
        $(option_enable cuda cuda-hwaccel)
        $(option_enable cuda cuda-interop)
        $(option_enable va vaapi)
        $(option_enable vdpau)
    )

    ewaf --jobs=1 configure \
       "${args[@]}"
}

mpv_src_install() {
    waf_src_install

    if option !bash-completion; then
        # Completion file is installed unconditionally it seems
        edo rm -fr "${IMAGE}"/usr/share/bash-completion
    fi

    if option !zsh-completion; then
        # Completion file is installed unconditionally it seems
        edo rm -fr "${IMAGE}"/usr/share/zsh
    fi
}

mpv_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

mpv_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

