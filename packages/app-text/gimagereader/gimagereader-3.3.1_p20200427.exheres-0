# Copyright 2016-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=manisandro pn=gImageReader tag=623622dd52758c49462a7f6d4154d4005679afbd ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Front-end to tesseract-ocr"
DESCRIPTION="
gImageReader is a simple front-end to tesseract. Features include:
 - Automatic page layout detection
 - User can manually define and adjust recognition regions
 - Import images from disk, scanning devices, clipboard and screenshots
 - Supports multipage PDF documents
 - Recognized text displayed directly next to the image
 - Editing of output text, including search/replace and removing line breaks
 - Spellchecking for output text (if corresponding dictionary installed)
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-arch/libzip
        app-arch/quazip
        app-spell/enchant:2
        app-text/djvu
        app-text/podofo
        app-text/poppler[qt5]
        app-text/qtspell[>=0.8.0]
        app-text/tesseract
        dev-cpp/libxml++:3.0
        media-gfx/sane-backends
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        sys-libs/libgomp:=
        x11-libs/qtbase:5[gui]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-3.3.1-Use-CMAKE_INSTALL_FULL_DATAROOTDIR.patch
    "${FILES}"/${PN}-3.2.3-fix-localesearch.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DINTERFACE_TYPE:STRING=qt5
    -DMANUAL_DIR:PATH=/usr/share/doc/${PNVR}
    -DENABLE_VERSIONCHECK:BOOL=FALSE
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

