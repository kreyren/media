# Copyright 2008 Richard Brown
# Copyright 2009 Bo Ørsted Andresen
# Copyright 2009 Thomas Anderson
# Copyright 2011 Ali Polatel
# Copyright 2011-2012 Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Copyright 2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson systemd-service

export_exlib_phases src_install

SUMMARY="A flexible, powerful, server-side application for playing music"
DESCRIPTION="
Music Player Daemon (MPD) is a flexible, powerful, server-side application for playing music.
Through plugins and libraries it can play a variety of sound files while being controlled by
its network protocol.
"
HOMEPAGE="https://www.musicpd.org"
DOWNLOADS="${HOMEPAGE}/download/${PN}/$(ever range -2)/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    aac
    alsa
    avahi
    cdio [[ description = [ CD support through libcdio ] ]]
    chromaprint [[ description = [ Support for finding AcoustID fingerprints of audio files ] ]]
    curl [[ description = [ Support obtaining song data via HTTP and enable the WebDAV storage plugin ] ]]
    dbus
    doc
    dsd [[ description = [ Support for decoding DSD ] ]]
    id3 [[ description = [ Support for ID3 tags ] ]]
    jack [[ description = [ Enable jack-audio-connection-kit audio output ] ]]
    libmpdclient [[ description = [ Enable support for remote mpd databases ] ]]
    libsamplerate
    mms [[ description = [ Microsoft Media Server protocol support ] ]]
    nfs [[ description = [ Enable support for streaming music over a NFS share ] ]]
    ogg
    openal
    opus [[ description = [ Opus codec support ] requires = ogg ]]
    pulseaudio
    qobuz [[
        description = [ Qobuz music streaming support (input) ]
        requires = [ curl yaml ]
    ]]
    samba [[ description = [ Enable support for streaming music over a SMB share ] ]]
    shout [[ description = [ Enable support for streaming through shout (mp3, and ogg if ogg is enabled) ] ]]
    sndfile
    sndio [[ description = [ Adds support for sound output through sndio
                             (OpenBSD sound API, also ported to Linux) ] ]]
    soundcloud [[
        description = [ SoundCloud.com support (input) ]
        requires = [ curl yaml ]
    ]]
    soxr [[ description = [ Enable support for the libsoxr resampler ] ]]
    sqlite [[ description = [ Enable support for storing the MPD database in an Sqlite database ] ]]
    systemd [[ description = [ systemd socket activation support ] ]]
    tidal [[
        description = [ TIDAL music streaming support (input) ]
        requires = [ curl yaml ]
    ]]
    udisks [[ description = [ Support for removable media using udisks2 ] ]]
    upnp [[ description = [ Support Service Discovery via UPnP ] ]]
    yaml [[ description = [ YAML support via libyajl ] ]]
    zip [[ description = [ zip archive support ] ]]
    (
        aac
        audiofile [[ description = [ Enable audiofile support, enables wave support ] ]]
        ffmpeg [[ description = [ Enable the ffmpeg input plugin, allowing you to play all audio formats supported by ffmpeg/libav ] ]]
        mikmod
        modplug [[ description = [ mod-like file format support ] ]]
        mp3
        musepack
        wavpack
        ( flac shout vorbis ) [[ requires = ogg ]]
    ) [[ number-selected = at-least-one ]]
    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( dev-python/Sphinx )
    build+run:
        app-arch/bzip2
        dev-libs/boost[>=1.54]
        dev-libs/expat
        dev-libs/icu:=
        dev-libs/pcre
        aac? ( media-libs/faad2 )
        alsa? ( sys-sound/alsa-lib[>=0.9.0] )
        audiofile? ( media-libs/audiofile[>=0.3] )
        avahi? ( net-dns/avahi )
        cdio? (
            dev-libs/libcdio
            dev-libs/libcdio-paranoia[>=0.93_p1]
        )
        chromaprint? ( media-libs/chromaprint )
        curl? ( net-misc/curl[>=7.33] )
        dbus? ( sys-apps/dbus )
        ffmpeg? (
            providers:ffmpeg? ( media/ffmpeg[>=3.1] )
            providers:libav? ( media/libav )
        )
        flac? ( media-libs/flac[>=1.2][ogg?] )
        id3? ( media-libs/libid3tag )
        jack? ( media-sound/jack-audio-connection-kit )
        libmpdclient? ( media-libs/libmpdclient[>=2.9] )
        libsamplerate? ( media-libs/libsamplerate[>=0.1.3] )
        mms? ( media-libs/libmms[>=0.4] )
        mikmod? ( media-libs/libmikmod[>=3.3.6] )
        modplug? ( media-libs/libmodplug )
        mp3? (
            media-libs/libmad
            media-sound/lame
        )
        musepack? ( media-libs/musepack )
        nfs? ( net-fs/libnfs )
        ogg? ( media-libs/libogg )
        openal? ( media-libs/openal )
        opus? ( media-libs/opus )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.16] )
        qobuz? ( dev-libs/libgcrypt )
        samba? ( net-fs/samba )
        shout? ( media-libs/libshout )
        sndfile? ( media-libs/libsndfile )
        sndio? ( sys-sound/sndio )
        soxr? ( media-libs/soxr )
        sqlite? ( dev-db/sqlite:3[>=3.7.3] )
        systemd? ( sys-apps/systemd )
        upnp? (
            net-libs/libupnp
        )
        vorbis? ( media-libs/libvorbis )
        wavpack? ( media-sound/wavpack )
        yaml? ( dev-libs/yajl[>=2.0] )
        zip? ( dev-libs/zziplib[>=0.13] )
    test:
        dev-cpp/gtest
    suggestion:
        media-sound/ario [[ description = [ Provides rhythmbox-like client ] ]]
        media-sound/gmpc [[ description = [ Provides fast and fully featured GTK-based client ] ]]
        media-sound/mpc [[ description = [ Provides command line client ] ]]
        media-sound/mpdcron [[ description = [ Executes scripts based on mpd's idle events ] ]]
        media-sound/ncmpc [[ description = [ Provides ncurses based command line client ] ]]
        media-sound/pms [[ description = [ Provides an alternative ncurses based command line client ] ]]
        media-sound/qmpdclient [[ description = [ Provides simple QT client ] ]]
        media-sound/sonata [[ description = [ Provides an elegant GTK-based client ] ]]
        sys-sound/oss [[ description = [ Provides an alternative sound architecture instead of ALSA ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dadplug=disabled
    -Dao=disabled
    -Dbzip2=enabled
    -Dcue=true
    -Ddaemon=true
    -Ddatabase=true
    -Depoll=true
    -Deventfd=true
    -Dexpat=enabled
    -Dfifo=true
    -Dfluidsynth=disabled
    -Dgme=disabled
    -Dhttpd=true
    -Diconv=disabled
    -Dicu=enabled
    -Dinotify=true
    -Dipv6=enabled
    -Diso9660=disabled
    -Dlocal_socket=true
    -Dmpg123=disabled
    -Dneighbor=true
    -Doss=enabled
    -Dpcre=enabled
    -Dpipe=true
    -Drecorder=true
    -Dshine=disabled
    -Dsidplay=disabled
    -Dsignalfd=true
    -Dsolaris_output=disabled
    -Dsyslog=enabled
    -Dtcp=true
    # libvorbis is preferred anyway, could be an option for embedded devices
    -Dtremor=disabled
    -Dtwolame=disabled
    -Dwave_encoder=true
    -Dwildmidi=disabled
    -Dzlib=enabled
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'avahi zeroconf avahi disabled'
    'doc documentation'
    dsd
    "systemd systemd_system_unit_dir ${SYSTEMDSYSTEMUNITDIR}"
    "systemd systemd_user_unit_dir ${SYSTEMDUSERUNITDIR}"
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'aac faad'
    alsa
    audiofile
    'cdio cdio_paranoia'
    chromaprint
    curl
    'curl webdav'
    dbus
    ffmpeg
    flac
    'id3 id3tag'
    jack
    libmpdclient
    libsamplerate
    mms
    mikmod
    modplug
    'mp3 mad'
    'mp3 lame'
    'musepack mpcdec'
    nfs
    openal
    opus
    'pulseaudio pulse'
    qobuz
    shout
    sndfile
    sndio
    'samba smbclient'
    soundcloud
    soxr
    sqlite
    systemd
    tidal
    udisks
    upnp
    vorbis
    'vorbis vorbisenc'
    wavpack
    'yaml yajl'
    'zip zzip'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtest=true -Dtest=false'
)

mpd_src_install() {
    meson_src_install

    # Don't install docs a second time into an undesired dir, emagicdocs
    # already installs them.
    edo rm "${IMAGE}"/usr/share/doc/${PN}/{AUTHORS,COPYING,NEWS,README.md}
    edo rmdir "${IMAGE}"/usr/share/doc/${PN}
}

