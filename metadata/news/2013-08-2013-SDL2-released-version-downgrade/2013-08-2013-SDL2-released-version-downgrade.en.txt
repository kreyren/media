Title: SDL 2.0 officially released, slot problems
Author: Lasse Brun <bruners@gmail.com>
Content-Type: text/plain
Posted: 2013-08-2013
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: media-libs/SDL:2[=2.0.0_7140]

SDL 2.0 has been officially released, the testing version in ::media is
considered greater by Paludis due to a bad choice of version string.

cave resolve media-libs/SDL --permit-downgrade media-libs/SDL

You might want to reinstall any packages that depend upon SDL afterwards.

Additionally SDL:2 will remain masked until all packages that depend
upon SDL-1.2 are properly slotted.
